# issue-351392 - Dynamic child pipeline jobs have no artifacts safeguard on retry

Reproducer for [#3351392](https://gitlab.com/gitlab-org/gitlab/-/issues/351392).

See https://gitlab.com/cboos/issue-351392/-/jobs/2025718281, a job that shouldn't have been allowed to run had the safeguard for missing artifacts been there.

See also !1, which demonstrates that the safeguard *is* present when these jobs in child pipelines are initially started.
